﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Portal.Areas.Shop.Controllers
{
    [Area("Shop")]
    public class ManageShopController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
